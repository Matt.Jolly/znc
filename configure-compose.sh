#!/bin/bash
# Copyright (C) 2022 Matt Jolly

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# GLPI Docker stack configuration script.
# This script will use `sed` to substitute replacement variables to configure the URI
# that you want your service to run on.

echo "Please enter the subdomain UNDER which you want the download stack to run."
echo "E.g. for 'znc.docker.domain.tld' enter 'docker.domain.tld'"

read DOMAIN


printf "\n"
echo "Configuring your Docker Compose file."
sed -i "s/«DOMAIN»/${DOMAIN}/g" docker-compose.yml

echo "Creating directories for bind mounts."

mkdir -p $(pwd)/mounts/config

echo "Done. You should be able to deploy the stack using:"
echo "'docker stack deploy -c docker-compose.yml znc'"
echo "It would be advisable to run the container once to create your configuration before deploying the stack:"
echo "'docker run -it -v $(pwd)/mounts/config:/znc-data znc --makeconf'"
